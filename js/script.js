const submit = document.getElementById("submit_button");
const tableContainer = document.getElementById("table_container");

submit.addEventListener("click", (e) => {
    let table = document.querySelector(".table");
    if(table !== null)
    {
        table.remove();
    }
    table = document.createElement("table");
    table.className = "table";
    const rows = document.getElementById("input_row").value;
    const columns = document.getElementById("input_column").value;
    for(let i = 0; i < rows; i ++)
    {   
        let newTr = document.createElement("tr");
        for(let j = 0; j < columns; j++)
        {
            let newTd = document.createElement("td");
            newTr.appendChild(newTd);
        }
        table.appendChild(newTr);
    }
    tableContainer.appendChild(table);
});

